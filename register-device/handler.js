'use strict';

var MongoClient = require('mongodb').MongoClient;
var url = "mongodb://ip-172-31-29-17.ec2.internal:27017/";

exports.registerDevice = (event, context, callback) => {
    var registrationId = event.registrationId;
    var deviceName = event.deviceName;
    var userId = event.userId;

    MongoClient.connect(url, function(err, db) {
        if (err) throw err;
        var selectedDb = db.db("home-controller-db");
        
        selectedDb.collection("deviceInfo").find({"registrationId":registrationId}).toArray(function(err, result){
            if (err) throw err;
            
            if(result){
              if(!result[0].registered){
                selectedDb.collection("deviceInfo").update(
                  result[0],
                  { $set : { "registered": true, "deviceName": deviceName, "userId": userId } }
                );
                db.close();
                callback(null, "Device registered!");
              }else{
                db.close();
                callback(null, "Device already registered!");
              }
            }
        });
      });
}